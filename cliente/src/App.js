import './App.css';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { Item } from './Item';

function App() {
  // 1.- Crear useState
  const [tareas, setTareas] = useState([]);

  // 2.- Crear useEffect
  useEffect(() => {
    //Petición get
    const obtenerTareas = async () => {
      const url = 'api';
      const result = await axios.get(url);
      setTareas(result.data);
    };
    obtenerTareas();
    //
  }, []);

  return (
    <div className='App'>
      <h1>Peticiones</h1>
      <ul>
        {tareas.map((tarea, i) => {
          return <Item data={tarea} key={tarea.id} />;
        })}
      </ul>
    </div>
  );
}

export default App;

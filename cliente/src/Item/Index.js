import React from "react";
import './item.css'

const Item = ({data})=>{
            return(
                <li key={data.id}>
                <p>{data.text}</p>
              </li>
            )
}

export {Item}
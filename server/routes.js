// Routes.js - Módulo de rutas
const express = require('express');
const router = express.Router();

const arrayObjects = [{
  id:1,
  text: "Brayan"
},
{
  id:2,
  text: "Jorge"
},
{
  id:3,
  text: "Felipe"
},
{
  id:4,
  text: "Sergio"
},
{
  id:5,
  text: "Juan"
}]

// Get mensajes
router.get('/', function (req, res) {
  res.json(arrayObjects);
});

//post mensaje
router.post('/', function (req, res) {
  const text = {
    text: req.body.tarea
  };

  arrayObjects.puch( text );
  console.log(arrayObjects);

  res.json({
    ok: true,
    text
  })
})

module.exports = router;
